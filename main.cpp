/*
 * main.cpp
 */

#include <sys/types.h>  // This provides typedefs needed by libgte.h and libgpu.h
#include <stdio.h>  // Not necessary but include it anyway
#include <string.h>
#include <psxetc.h> // Includes some functions that controls the display
#include <psxgte.h> // GTE header, not really used but libgpu.h depends on it
#include <psxgpu.h> // GPU library header
#include <psxpad.h>

#include <System.h>
#include <Pad.h>
#include <Model.h>
#include <Model_textured.h>
#include <Cube.h>
#include <Cube_textured.h>
#include <Texture.h>
#include <Light.h>
#include <Camera.h>

#include "car.h"

#define ever ;;

MeshData carData = {
    .vertices = carVertices,
    .normals = carNormals,
    .indices = carIndices,
    .texCoords = NULL,
    .numVertices = numCarVertices, // TODO - add this calculation to ply converter code
    .numNormals = numCarNormals,
    .numIndices = numCarIndices,
    .numTexcoords = 0
};

int main(void)
{
    SVECTOR lightColor = {ONE,ONE,ONE,0};
    SVECTOR lightDir = {-2048,-2048,-2048,0};
    
    Light light;
    light.setColor(&lightColor, 0);
    light.setDirection(&lightDir, 0);

    Cube_textured cube;
    cube.translate(0, 0, 500);

    Model car;

    Camera camera;
    camera.translate(0, 250, -550);
    camera.rotate(-150, 0, 0);

    System::Init();
    Pad::Init();
    Texture::loadDefaultTexture();

    car.load(&carData);
    car.rotate(ONE >> 2, 0, ONE >> 2); // ONE = 360 degrees -> ONE/4 = 90

    for(ever)
    {
        static MATRIX modelTransformMat;
        cube.getTransformMat(&modelTransformMat);

        light.multiplyMat(&modelTransformMat);
        light.use();

        Pad::Update();
        // move
        if (Pad::IsHeld( PAD_RIGHT )) {
            //car.translate(5, 0, 0);
            //camera.translateAbsolute(5, 0, 0);
            camera.translate(5, 0, 0);
        }
        if (Pad::IsHeld( PAD_LEFT )) {
            //car.translate(-5, 0, 0);
            //camera.translateAbsolute(-5, 0, 0);
            camera.translate(-5, 0, 0);
        }
        if (Pad::IsHeld( PAD_UP )) {
            //car.translate(0, 0, 5);
            //camera.translateAbsolute(0, 0, 5);
            camera.translate(0, 0, 5);
        }
        if (Pad::IsHeld( PAD_DOWN )) {
            //car.translate(0, 0, -5);
            //camera.translateAbsolute(0, 0, -5);
            camera.translate(0, 0, -5);
        }
        // rotate
        if (Pad::IsHeld( PAD_TRIANGLE)) {
            camera.rotate(8, 0, 0);
            printf("Triangle\n");
        }
        if (Pad::IsHeld( PAD_CROSS )) {
            camera.rotate(-8, 0, 0);
            printf("Cross\n");
        }
        if (Pad::IsHeld( PAD_SQUARE )) {
            //car.rotate(0, 0, 8);
            camera.rotate(0, 8, 0);
            printf("Square\n");
        }
        if (Pad::IsHeld( PAD_CIRCLE )) {
            //car.rotate(0, 0, -8);
            camera.rotate(0, -8, 0);
            printf("Circle\n");
        }

        // test car rotation
        /*if (Pad::IsHeld(PAD_L1)) {
            car.rotate(0, 8, 0);
        }
        if (Pad::IsHeld(PAD_L2)) {
            car.rotate(0, -8, 0);
        }*/

        static MATRIX viewMat;
        camera.getViewMat(&viewMat);

        cube.rotate(25, 10, 5);
        cube.draw(&viewMat);
        
        car.getTransformMat(&modelTransformMat);
        light.multiplyMat(&modelTransformMat); 
        light.use();
        car.draw(&viewMat);

        System::Display();
    }

    return 0;
}

